### Background Color Tool

This is a screen color picker and solid background setter for X11 with a Motif GUI. It uses 'feh' and the ImageMagick 'convert' commands to turn the selected color into a small image in /tmp that gets tiled across the desktop root window. 

The 'feh' command automatically adds the full image path to the ~/.fehbg script. The image in /tmp might get lost so this will get updated at some point to store it in the home dir.


![screenshot](screenshots/dvbgcolor_sample.png)
![screenshot](screenshots/dvbgcolor_sample2.png)

#### Features

- Select colors from a list or with RGB sliders.
- Grab colors from anywhere on screen.
- Apply selected color as desktop background.

#### Requirements 

- Motif 2.3.8
- X11/Xlib
- ImageMagick
- feh

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvbgcolor
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

#### Credits

Big thank you to Hugo Peixoto on GitHub for colorpick.c (MIT licensed). The grabc.h file is a modified version of that demo.


#### License

This software is distributed free of charge under the BSD Zero Clause license.
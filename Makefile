CC = cc
CFLAGS = -O3 -Wno-unused-variable -Wno-unused-parameter -Wno-incompatible-pointer-types
LDFLAGS = -lXm -lX11 -lXt -lXinerama
TARGET = ./dvbgcolor
INSTDIR = /usr/local/bin

.PHONY: all clean install uninstall

all: $(TARGET)

$(TARGET): dvbgcolor.c
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

clean:
	rm -f $(TARGET)

install: $(TARGET)
	install -m 755 $(TARGET) $(INSTDIR)

uninstall:
	rm -f $(INSTDIR)/$(notdir $(TARGET))

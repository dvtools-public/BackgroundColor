/* Modded version of colorpick.c by Hugo Peixoto on GitHub. License is MIT. */

/*
Copyright 2017 Hugo Peixoto <hugo.peixoto@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/
void grabColorFromScreen(Widget widget, XtPointer client_data)
{
	int x = 0;
	int y = 0;
	
	Display* display = NULL;
	display = XOpenDisplay(NULL);
	Window root;
	root = DefaultRootWindow(display);
	Cursor cursor;
	cursor = XCreateFontCursor(display, XC_dotbox); /* use dotbox pixmap */
	XEvent event;
		
/* grab pointer and turn it into dotbox icon */
	XGrabPointer(display, root, False, ButtonPressMask, GrabModeAsync, GrabModeAsync, None, cursor, CurrentTime);

	while(1) /* while pointer is grabbed... */
	{
		XNextEvent(display, &event); /* wait for next input event from mouse */

		if(event.type == ButtonPress)
		{
			x = event.xbutton.x;
			y = event.xbutton.y;
			
			break;
		}
	}

/* get color of pixel under cursor */
	XImage* image = NULL;
	image = XGetImage(display, root, x, y, 1, 1, AllPlanes, ZPixmap);
	XColor color;
	color.pixel = XGetPixel(image, 0, 0);
	XQueryColor(display, DefaultColormap(display, DefaultScreen(display)), &color);

/* string print and format so the colorSelector can interpret and apply the value */
	char colorName[32];
	sprintf(colorName, "#%02X%02X%02X", color.red >> 8, color.green >> 8, color.blue >> 8);

/* apply colorName to colorSelector in the main function */
	Widget colorSelector = (Widget)client_data;
	XtVaSetValues(colorSelector, XmNcolorName, &colorName, NULL);
	
/* clean up */
	XDestroyImage(image);
	XUngrabPointer(display, CurrentTime);
	XFreeCursor(display, cursor);
	XCloseDisplay(display);
}







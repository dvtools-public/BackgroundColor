/*
	BSD Zero Clause License (0BSD)
	
	Permission to use, copy, modify, and/or distribute this 
	software for any purpose with or without fee is hereby 
	granted.

	THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS 
	ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL 
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO 
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
	INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
	WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
	TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH 
	THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>
#include <Xm/MainW.h>
#include <Xm/Form.h>
#include <Xm/ColorS.h>
#include <Xm/Separator.h>
#include <Xm/PushB.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>

#include "grabc.h"

/* fallback x11 resources */
static String fallbacks[] = 
{
	/* some optional font settings */
	"*renderTable: xft",
	"*xft*fontType: FONT_IS_XFT",
	"*xft*fontName: Roboto",
	"*xft*fontSize: 11",
	"*xft*autohint: 1",
	"*xft*lcdfilter: lcddefault",
	"*xft*hintstyle: hintslight",
	"*xft*hinting: True",
	"*xft*antialias: 1",
	"*xft*rgba: rgb",
	"*xft*dpi: 96",
	
	
	
	"*XmToggleButton*shadowThickness: 0",
	"*XmToggleButton*highlightThickness: 0",
	"*colorSelector*colorName: Aquamarine4",
	"*colorSelector*colorMode: ListMode",
	"*colorSelector*colorListTogLabel: X11 Colors",
	"*colorSelector*sliderTogLabel: RGB Sliders",
	"*colorFrame*XmLabel*fontSize: 12",
	/* "*colorFrame*XmLabel*fontStyle: bold", */
	"*scale*Title*fontStyle: italic",
	"*XmScrollBar*sliderMark: XmNONE",
	"*XmScrollBar*showArrows: XmNONE",
	"*list*highlightThickness: 0",
	"*list*listMarginHeight: 3",
	"*list*listMarginWidth: 5",
	
	
	"dvbgcolor*XmScrollBar*sliderMark: XmNONE",
	"dvbgcolor*XmScrollBar*showArrows: XmNONE",
	"dvbgcolor*XmScrollBar*width: 10",
	"dvbgcolor*colorFrame*shadowType: XmSHADOW_IN",
	
	
	NULL,
};

void generateImageFromHex(const char *hexColor)
{
	char command[256];
/* use imagemagick convert tool to make a 2x2 png in the /tmp directory */
	snprintf(command, sizeof(command), "convert -size 2x2 xc:\"%s\" /tmp/dvbgtool_tmp.png", hexColor);
	system(command);
}

void printColorNameCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	Widget colorSelector = (Widget)client_data;
	char *color_name;
/* get color name from colorSelector widget */
	XtVaGetValues(colorSelector, XmNcolorName, &color_name, NULL);
	const char *hexColor = color_name;
/* generate png using color name */
	generateImageFromHex(hexColor);
/* set output image as desktop background using feh */
	system("feh --bg-tile /tmp/dvbgtool_tmp.png");
}

void quitCallback() { exit(0); }


int main(int argc, char *argv[])
{
	XtAppContext app;

/* open top level shell with initial resources */
	Widget topLevel = XtVaAppInitialize(&app, "dvbgcolor", NULL, 0, &argc, argv, fallbacks, NULL);
	XtVaSetValues(topLevel,
		XmNtitle, "Select Background Color",
		XmNheight, 406,
	NULL);
	
/* set window decorations and functions through vendorshell */
	int decor;
	XtVaGetValues(topLevel, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_BORDER;
		decor &= ~MWM_DECOR_TITLE;
		decor &= ~MWM_DECOR_MENU;
	XtVaSetValues(topLevel, XmNmwmDecorations, decor, NULL);
	
	int func;
	XtVaGetValues(topLevel, XmNmwmFunctions, &func, NULL);
		func &= ~MWM_FUNC_CLOSE;
		func &= ~MWM_FUNC_MOVE;
	XtVaSetValues(topLevel, XmNmwmFunctions, func, NULL);
	
/* create main window */
	Widget mainWin = XtVaCreateManagedWidget("mainWin", xmMainWindowWidgetClass, topLevel, NULL);
	XtVaSetValues(mainWin,
		XmNshadowThickness, 0,
	NULL);
	
/*create main form */
	Widget mainForm = XtVaCreateManagedWidget("mainForm", xmFormWidgetClass, mainWin, NULL);
	XtVaSetValues(mainForm,
		XmNshadowThickness, 1,
	NULL);
	
/* create color selector widget */
	Widget colorSelector = XtVaCreateManagedWidget("colorSelector", xmColorSelectorWidgetClass, mainForm, NULL);
	XtVaSetValues(colorSelector,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 6,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 6,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
	NULL);
	
/* create separator */
	Widget sep = XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, mainForm,
		XmNheight, 2,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 46,
	NULL);

/* create push buttons under separator */
	Widget applyButton = XtVaCreateManagedWidget("Apply", xmPushButtonWidgetClass, mainForm,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNmarginWidth, 10,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomOffset, 8,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 6,
		XmNleftOffset, 20,
	NULL);
	
	XtAddCallback(applyButton, XmNactivateCallback, printColorNameCallback, colorSelector);
	
	Widget cancelButton = XtVaCreateManagedWidget("Cancel", xmPushButtonWidgetClass, mainForm,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNmarginWidth, 8,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, applyButton,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightOffset, 5,
		XmNbottomOffset, 8,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 6,
		XmNleftOffset, 20,
	NULL);
	
	XtAddCallback(cancelButton, XmNactivateCallback, quitCallback, colorSelector);
	
	Widget pickButton = XtVaCreateManagedWidget("Grab Pixel Color", xmPushButtonWidgetClass, mainForm,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNmarginWidth, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightOffset, 5,
		XmNbottomOffset, 8,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 6,
		XmNleftOffset, 8,
	NULL);
	
	XtAddCallback(pickButton, XmNactivateCallback, grabColorFromScreen, colorSelector);
	
	/* fix attachments */
	XtVaSetValues(cancelButton,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, pickButton,
		XmNleftOffset, 5,
	NULL);
	
/* attach separator to color selector after widget creation */	
	XtVaSetValues(colorSelector,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, sep,
		XmNbottomOffset, 8,
		XmNheight, 400,
	NULL);
	
	
	XtRealizeWidget(topLevel);

/* give color selector widget default focus */
	XmProcessTraversal(colorSelector, XmTRAVERSE_CURRENT);

	XtAppMainLoop(app);
	return(0);
}
